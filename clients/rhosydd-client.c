/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib-unix.h>
#include <glib/gi18n.h>
#include <gio/gio.h>
#include <locale.h>
#include <signal.h>

#include "librhosydd/attribute.h"
#include "librhosydd/proxy-vehicle.h"
#include "librhosydd/subscription.h"
#include "librhosydd/vehicle.h"
#include "librhosydd/vehicle-manager.h"

/* Exit statuses. */
typedef enum
{
  /* Success. */
  EXIT_OK = 0,
  /* Error parsing command line options. */
  EXIT_INVALID_OPTIONS = 1,
  /* Bus or well-known name unavailable. */
  EXIT_BUS_UNAVAILABLE = 2,
  /* Command failed. */
  EXIT_FAILED = 3,
} ExitStatus;

/* Commands. @args is the remainder of the command line arguments following
 * the command name (which is removed). @task is a #GTask which the command
 * function must call g_task_return_*() on once complete. */
typedef void (*CommandFunc) (RsdVehicleManager   *manager,
                             const gchar * const *args,
                             GTask               *task);

typedef struct
{
  const gchar *name;
  CommandFunc func;
  const gchar *description;
} CommandData;

static gchar *
list_available_commands (const CommandData *commands,
                         gsize              n_commands)
{
  GString *out = NULL;
  gsize i;
  gint max_width = 0;

  /* Work out the width of the longest command name. */
  for (i = 0; i < n_commands; i++)
    max_width = MAX (max_width, (gint) strlen (commands[i].name));

  /* Build the output. */
  out = g_string_new ("");
  g_string_append_printf (out, "%s\n", _("Commands:"));

  for (i = 0; i < n_commands; i++)
    {
      g_string_append_printf (out, "  %*s  %s\n", max_width, commands[i].name,
                              gettext (commands[i].description));
    }

  return g_string_free (out, FALSE);
}

/* A way of automatically removing sources when going out of scope. */
typedef guint SourceId;
G_DEFINE_AUTO_CLEANUP_FREE_FUNC (SourceId, g_source_remove, 0)

/* Utility functions. */
static const gchar *
format_availability_flags (RsdAttributeAvailability availability,
                           RsdAttributeFlags        flags)
{
  switch (availability)
    {
    case RSD_ATTRIBUTE_AVAILABLE:
      break;
    case RSD_ATTRIBUTE_NOT_SUPPORTED:
      return "NS";
    case RSD_ATTRIBUTE_NOT_SUPPORTED_YET:
      return "NS-Y";
    case RSD_ATTRIBUTE_NOT_SUPPORTED_SECURITY_POLICY:
      return "NS-SP";
    case RSD_ATTRIBUTE_NOT_SUPPORTED_BUSINESS_POLICY:
      return "NS-BP";
    case RSD_ATTRIBUTE_NOT_SUPPORTED_OTHER:
      return "NS-O";
    default:
      break;
    }

  if ((flags & RSD_ATTRIBUTE_READABLE) && (flags & RSD_ATTRIBUTE_WRITABLE))
    return "RW";
  else if (flags & RSD_ATTRIBUTE_READABLE)
    return "R";
  else if (flags & RSD_ATTRIBUTE_WRITABLE)
    return "W";
  else
    return "-";
}

/*
 * format_timestamp:
 * @timestamp: timestamp to format (in microseconds)
 * @current_time: current time in the same clock domain as @timestamp (in
 *    microseconds)
 *
 * Format the given @timestamp relative to the @current_time. Both timestamps
 * must be in the same clock domain, but since they are considered relative to
 * each other, this clock domain does not have to be related to the system clock
 * or any other clock domain.
 *
 * Returns: (transfer full): human-readable formatted timestamp
 */
static gchar *
format_timestamp (RsdTimestampMicroseconds timestamp,
                  RsdTimestampMicroseconds current_time)
{
  if (timestamp == RSD_TIMESTAMP_UNKNOWN)
    return _("unknown time");

  return g_strdup_printf ("%" G_GINT64_FORMAT "µs ago",
                          current_time - timestamp);
}

/* Command implementations. */
/* list-vehicles */
static void
command_list_vehicles (RsdVehicleManager   *manager,
                       const gchar * const *args,
                       GTask               *task)
{
  g_autoptr (GPtrArray/*<owned RsdVehicle>*/) vehicles = NULL;
  guint i;

  vehicles = rsd_vehicle_manager_list_vehicles (manager);

  for (i = 0; i < vehicles->len; i++)
    {
      RsdVehicle *vehicle = RSD_VEHICLE (vehicles->pdata[i]);

      g_print ("%s\n", rsd_vehicle_get_id (vehicle));
    }

  g_task_return_boolean (task, TRUE);
}

static RsdVehicle *
args_get_vehicle (RsdVehicleManager    *manager,
                  const gchar * const  *args,
                  guint                 idx,
                  GError              **error)
{
  const gchar *vehicle_id;
  g_autoptr (RsdVehicle) vehicle = NULL;

  if (g_strv_length ((gchar **) args) < idx + 1)
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                   _("Missing vehicle ID."));
      return NULL;
    }

  vehicle_id = args[idx];

  if (!rsd_vehicle_id_is_valid (vehicle_id))
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                   _("Invalid vehicle ID ‘%s’."), vehicle_id);
      return NULL;
    }

  vehicle = rsd_vehicle_manager_get_vehicle (manager, vehicle_id);

  if (vehicle == NULL)
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND,
                   _("Unknown vehicle ‘%s’."), vehicle_id);
      return NULL;
    }

  return g_steal_pointer (&vehicle);
}

static const gchar *
args_get_attribute_name (const gchar * const  *args,
                         guint                 idx,
                         GError              **error)
{
  const gchar *attribute_name;

  if (g_strv_length ((gchar **) args) < idx + 1)
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                   _("Missing attribute name."));
      return NULL;
    }

  attribute_name = args[idx];

  if (!rsd_attribute_name_is_valid (attribute_name))
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                   _("Invalid attribute name ‘%s’."), attribute_name);
      return NULL;
    }

  return attribute_name;
}

/* show-vehicle */
static void show_vehicle_cb (GObject      *obj,
                             GAsyncResult *result,
                             gpointer      user_data);

static void
command_show_vehicle (RsdVehicleManager   *manager,
                      const gchar * const *args,
                      GTask               *task)
{
  g_autoptr (RsdVehicle) vehicle = NULL;
  GError *error = NULL;

  vehicle = args_get_vehicle (manager, args, 0, &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  /* Query for the vehicle’s details. */
  rsd_vehicle_get_attributes_async (vehicle, "",
                                    g_task_get_cancellable (task),
                                    show_vehicle_cb, task);
}

static void
show_vehicle_cb (GObject      *obj,
                 GAsyncResult *result,
                 gpointer      user_data)
{
  RsdVehicle *vehicle = RSD_VEHICLE (obj);
  GTask *task = G_TASK (user_data);
  g_autoptr (GPtrArray/*<RsdAttributeInfo>*/) attributes = NULL;
  RsdTimestampMicroseconds current_time;
  GError *error = NULL;
  guint i;

  attributes = rsd_vehicle_get_attributes_finish (vehicle, result,
                                                  &current_time, &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  if (!attributes->len)
     g_print ("(No attributes.)\n");

  for (i = 0; i < attributes->len; i++)
    {
      const RsdAttributeInfo *info = attributes->pdata[i];
      g_autofree gchar *formatted_update_timestamp = NULL;
      g_autofree gchar *formatted_attribute_value = NULL;
      const gchar *formatted_availability_flags;

      formatted_update_timestamp = format_timestamp (info->attribute.last_updated,
                                                     current_time);
      formatted_attribute_value = g_variant_print (info->attribute.value, FALSE);

      formatted_availability_flags = format_availability_flags (info->metadata.availability,
                                                                info->metadata.flags);

      g_print ("%s = %s±%f (%s, last updated: %s)\n",
               info->metadata.name,
               formatted_attribute_value,
               info->attribute.accuracy, 
               formatted_availability_flags,
               formatted_update_timestamp);
    }
  g_task_return_boolean (task, TRUE);
}

/* show-vehicle-metadata */
static void show_vehicle_metadata_cb (GObject      *obj,
                                      GAsyncResult *result,
                                      gpointer      user_data);

static void
command_show_vehicle_metadata (RsdVehicleManager   *manager,
                               const gchar * const *args,
                               GTask               *task)
{
  g_autoptr (RsdVehicle) vehicle = NULL;
  GError *error = NULL;

  vehicle = args_get_vehicle (manager, args, 0, &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  /* Query for the vehicle’s metadata. */
  rsd_vehicle_get_metadata_async (vehicle, "",
                                  g_task_get_cancellable (task),
                                  show_vehicle_metadata_cb, task);
}

static void
show_vehicle_metadata_cb (GObject      *obj,
                          GAsyncResult *result,
                          gpointer      user_data)
{
  RsdVehicle *vehicle = RSD_VEHICLE (obj);
  GTask *task = G_TASK (user_data);
  g_autoptr (GPtrArray/*<owned RsdAttributeMetadata>*/) metadatas = NULL;
  GError *error = NULL;
  guint i;

  metadatas = rsd_vehicle_get_metadata_finish (vehicle, result, NULL,
                                               &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  if (!metadatas->len)
     g_print ("(No attributes.)\n");

  for (i = 0; i < metadatas->len; i++)
    {
      const RsdAttributeMetadata *metadata = metadatas->pdata[i];
      g_autofree gchar *formatted_metadata = g_variant_print(metadata->vss_metadata, TRUE);
      const gchar *formatted_availability_flags;

      formatted_availability_flags = format_availability_flags (metadata->availability,
                                                                metadata->flags);

      g_print ("%s (%s): %s\n", metadata->name, 
               formatted_availability_flags, formatted_metadata );
    }
  g_task_return_boolean (task, TRUE);
}

/* get-attribute */
static void get_attribute_cb (GObject      *obj,
                              GAsyncResult *result,
                              gpointer      user_data);

static void
command_get_attribute (RsdVehicleManager   *manager,
                       const gchar * const *args,
                       GTask               *task)
{
  const gchar *attribute_name;
  g_autoptr (RsdVehicle) vehicle = NULL;
  GError *error = NULL;

  if (g_strv_length ((gchar **) args) != 2)
    {
      g_task_return_new_error (task, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                               _("Missing vehicle ID, or attribute name."));
      return;
    }

  vehicle = args_get_vehicle (manager, args, 0, &error);
  if (error == NULL)
    attribute_name = args_get_attribute_name (args, 1, &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  rsd_vehicle_get_attributes_async (vehicle,  attribute_name,
                                   g_task_get_cancellable (task),
                                   get_attribute_cb, task);
}

static void
get_attribute_cb (GObject      *obj,
                  GAsyncResult *result,
                  gpointer      user_data)
{
  RsdProxyVehicle *vehicle = RSD_PROXY_VEHICLE (obj);
  GTask *task = G_TASK (user_data);
  g_autoptr (GPtrArray) attributes = NULL;
  GError *error = NULL;
  guint i = 0;
  RsdTimestampMicroseconds current_time;

  attributes = rsd_vehicle_get_attributes_finish (RSD_VEHICLE(vehicle), result,
                                                  &current_time, &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  for (i = 0; i < attributes->len; i++)
    {
      RsdAttributeInfo *info = attributes->pdata[i];
      g_autofree gchar *formatted_update_timestamp = NULL;
      g_autofree gchar *formatted_attribute_value = NULL;
      const gchar *formatted_availability_flags;

      formatted_update_timestamp = format_timestamp (info->attribute.last_updated,
                                                     current_time);
      formatted_attribute_value = g_variant_print (info->attribute.value, TRUE);
      formatted_availability_flags = format_availability_flags (info->metadata.availability,
                                                                info->metadata.flags);
      g_print ("%s = %s±%f (%s, last updated: %s)\n",
               info->metadata.name,
               formatted_attribute_value,
               info->attribute.accuracy,
               formatted_availability_flags,
               formatted_update_timestamp);
    }
  g_task_return_boolean (task, TRUE);
}

/* get-attribute-metadata */
static void get_attribute_metadata_cb (GObject      *obj,
                                       GAsyncResult *result,
                                       gpointer      user_data);

static void
command_get_attribute_metadata (RsdVehicleManager   *manager,
                                const gchar * const *args,
                                GTask               *task)
{
  const gchar *attribute_name;
  g_autoptr (RsdVehicle) vehicle = NULL;
  GError *error = NULL;

  if (g_strv_length ((gchar **) args) != 2)
    {
      g_task_return_new_error (task, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                               _("Missing vehicle ID or object path "
                                 "or attribute name."));
      return;
    }

  vehicle = args_get_vehicle (manager, args, 0, &error);
  if (error == NULL)
    attribute_name = args_get_attribute_name (args, 1, &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  rsd_vehicle_get_metadata_async (vehicle, attribute_name,
                                  g_task_get_cancellable (task),
                                  get_attribute_metadata_cb, task);
}

static void
get_attribute_metadata_cb (GObject      *obj,
                           GAsyncResult *result,
                           gpointer      user_data)
{
  RsdVehicle *vehicle = RSD_VEHICLE (obj);
  GTask *task = G_TASK (user_data);
  g_autoptr (GPtrArray) metadatas = NULL;
  GError *error = NULL;
  guint i = 0;

  metadatas = rsd_vehicle_get_metadata_finish (vehicle, result, NULL,
                                               &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  if (!metadatas->len)
     g_print ("(No attributes.)\n");

  for (i = 0; i < metadatas->len; i++)
    {
      const RsdAttributeMetadata *metadata = metadatas->pdata[i];
      g_autofree gchar *formatted_metadata = g_variant_print(metadata->vss_metadata, TRUE);
      const gchar *formatted_availability_flags;

      formatted_availability_flags = format_availability_flags (metadata->availability,
                                                                metadata->flags);

      g_print ("%s (%s): %s\n", metadata->name,
               formatted_availability_flags, formatted_metadata);
    }
  g_task_return_boolean (task, TRUE);
}

/* monitor-vehicle */
static void     monitor_vehicle_cb             (GObject      *obj,
                                                GAsyncResult *result,
                                                gpointer      user_data);
static gboolean monitor_signal_cb              (gpointer    user_data);
static void     invalidated_cb                 (RsdVehicle   *vehicle,
                                                const GError *error,
                                                gpointer      user_data);
static void     attributes_changed_cb          (RsdVehicle               *vehicle,
                                                RsdTimestampMicroseconds  current_time,
                                                GPtrArray                *changed_attributes,
                                                GPtrArray                *invalidated_attributes,
                                                gpointer                  user_data);
static void     attributes_metadata_changed_cb (RsdVehicle               *vehicle,
                                                RsdTimestampMicroseconds  current_time,
                                                GPtrArray                *changed_attributes,
                                                gpointer                  user_data);

static void
command_monitor_vehicle (RsdVehicleManager   *manager,
                         const gchar * const *args,
                         GTask               *task)
{
  g_autoptr (RsdVehicle) vehicle = NULL;
  GError *error = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) unsubscriptions = NULL;

  if (g_strv_length ((gchar **) args) != 1)
    {
      g_task_return_new_error (task, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                               _("Missing vehicle ID or object path."));
      return;
    }

  vehicle = args_get_vehicle (manager, args, 0, &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  g_signal_connect (vehicle, "attributes-changed",
                    (GCallback) attributes_changed_cb, NULL);
  g_signal_connect (vehicle, "attributes-metadata-changed",
                    (GCallback) attributes_metadata_changed_cb, NULL);

  subscriptions = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  unsubscriptions = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);

  g_ptr_array_add (subscriptions, rsd_subscription_new_wildcard ());

  rsd_vehicle_update_subscriptions_async (vehicle, subscriptions,
                                          unsubscriptions,
                                          g_task_get_cancellable (task),
                                          monitor_vehicle_cb, task);
}

static void
monitor_vehicle_cb (GObject      *obj,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  RsdVehicle *vehicle = RSD_VEHICLE (obj);
  GTask *task = G_TASK (user_data);
  GError *error = NULL;

  rsd_vehicle_update_subscriptions_finish (vehicle, result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  /* Continue monitoring for ever, until the user cancels or the vehicle is
   * invalidated. */
  g_task_set_task_data (task, g_object_ref (vehicle), g_object_unref);
  g_unix_signal_add (SIGINT, monitor_signal_cb, task);
  g_signal_connect (vehicle, "invalidated", (GCallback) invalidated_cb, task);
}

static gboolean
monitor_signal_cb (gpointer user_data)
{
  GTask *task = G_TASK (user_data);

  g_task_return_boolean (task, TRUE);

  return G_SOURCE_REMOVE;
}

static void
invalidated_cb (RsdVehicle   *vehicle,
                const GError *error,
                gpointer      user_data)
{
  GTask *task = G_TASK (user_data);

  g_task_return_error (task, g_error_copy (error));
}

static void
attributes_changed_cb (RsdVehicle               *vehicle,
                       RsdTimestampMicroseconds  current_time,
                       GPtrArray                *changed_attributes,
                       GPtrArray                *invalidated_attributes,
                       gpointer                  user_data)
{
  gsize i;

  /* Changed attributes. */
  for (i = 0; i < changed_attributes->len; i++)
    {
      const RsdAttributeInfo *info = changed_attributes->pdata[i];
      g_autofree gchar *formatted_update_timestamp = NULL;
      g_autofree gchar *formatted_attribute_value = NULL;
      g_autofree gchar *formatted_metadata = NULL;
      const gchar *formatted_availability_flags;

      formatted_update_timestamp = format_timestamp (info->attribute.last_updated,
                                                     current_time);
      formatted_attribute_value = g_variant_print (info->attribute.value, TRUE);
      formatted_metadata = g_variant_print(info->metadata.vss_metadata, TRUE);
  
      formatted_availability_flags = format_availability_flags (info->metadata.availability,
                                                                info->metadata.flags);

      g_print ("%s = %s±%f ((%s)%s, last updated: %s)\n",
               info->metadata.name,
               formatted_attribute_value,
               info->attribute.accuracy,
               formatted_metadata,
               formatted_availability_flags,
               formatted_update_timestamp);
    }

  /* Invalidated attributes. */
  for (i = 0; i < invalidated_attributes->len; i++)
    {
      const RsdAttributeMetadata *metadata = invalidated_attributes->pdata[i];
      g_autofree gchar *formatted_metadata = NULL;
      const gchar *formatted_availability_flags;

      formatted_metadata = g_variant_print (metadata->vss_metadata, TRUE); 
      formatted_availability_flags = format_availability_flags (metadata->availability,
                                                                metadata->flags);

      g_print ("Invalidated attribute %s(%s): %s\n", metadata->name,
                formatted_availability_flags, formatted_metadata);
    }
}

static void
attributes_metadata_changed_cb (RsdVehicle               *vehicle,
                                RsdTimestampMicroseconds  current_time,
                                GPtrArray                *changed_attributes,
                                gpointer                  user_data)
{
  gsize i;

  /* Invalidated attributes. */
  for (i = 0; i < changed_attributes->len; i++)
    {
      const RsdAttributeMetadata *metadata = changed_attributes->pdata[i];
      g_autofree gchar *formatted_metadata = NULL;
      const gchar *formatted_availability_flags;

      formatted_metadata = g_variant_print (metadata->vss_metadata, TRUE);

      formatted_availability_flags = format_availability_flags (metadata->availability,
                                                                metadata->flags);
      g_print ("%s(%s): %s \n",  metadata->name,
               formatted_availability_flags, formatted_metadata);
    }
}

/* Main function stuff. */
typedef struct
{
  GCancellable *cancellable;  /* owned */
  gint signum;
} SignalData;

static void
signal_data_clear (SignalData *data)
{
  g_clear_object (&data->cancellable);
}

G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC (SignalData, signal_data_clear)

static gboolean
handle_signal (SignalData *signal_data,
               gint        signum)
{
  if (signal_data->cancellable != NULL)
    {
      signal_data->signum = signum;
      g_cancellable_cancel (signal_data->cancellable);
    }

  /* Remove the signal handler so when we raise again later, we don’t enter a
   * loop. */
  return G_SOURCE_REMOVE;
}

static gboolean
signal_sigint_cb (gpointer user_data)
{
  SignalData *signal_data = user_data;

  return handle_signal (signal_data, SIGINT);
}

static gboolean
signal_sigterm_cb (gpointer user_data)
{
  SignalData *signal_data = user_data;

  return handle_signal (signal_data, SIGTERM);
}

static void
vehicle_manager_new_cb (GObject      *obj,
                        GAsyncResult *result,
                        gpointer      user_data)
{
  GAsyncResult **result_out = user_data;

  *result_out = g_object_ref (result);
}

int
main (int   argc,
      char *argv[])
{
  g_autoptr (GOptionContext) context = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GDBusConnection) connection = NULL;
  g_autoptr (GCancellable) cancellable = NULL;
  g_auto (SourceId) sigint_id = 0, sigterm_id = 0;
  gsize i;
  g_autofree gchar *commands_help = NULL;
  g_autoptr (GTask) command_task = NULL;
  g_autoptr (GAsyncResult) async_result = NULL;
  g_autoptr (RsdVehicleManager) manager = NULL;
  g_auto (SignalData) signal_data = { NULL, 0 };

  /* Command line parameters. */
  g_autofree gchar *bus_address = NULL;
  g_auto (GStrv) args = NULL;

  const GOptionEntry entries[] =
  {
    { "bus-address", 'a', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &bus_address,
      N_("Address of the SDK D-Bus daemon to connect to (default: system "
         "bus)"),
      N_("ADDRESS") },
    { G_OPTION_REMAINING, 0, G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING_ARRAY,
      &args, NULL, NULL },
    { NULL, },
  };

  /* Commands. */
  const CommandData commands[] =
  {
    { "list-vehicles", command_list_vehicles,
      N_("List the IDs of all vehicles known to the system") },
    { "show-vehicle", command_show_vehicle,
      N_("Show the attributes for a specific vehicle") },
    { "show-vehicle-metadata", command_show_vehicle_metadata,
      N_("Show the metadata of the attributes for a specific vehicle") },
    { "get-attribute", command_get_attribute,
      N_("Get the value of a specific attribute in a  vehicle") },
    { "get-attribute-metadata", command_get_attribute_metadata,
      N_("Get the metadata for a specific attribute in a vehicle") },
    { "monitor-vehicle", command_monitor_vehicle,
      N_("Monitor for attribute changes on a specific vehicle") },
  };

  /* Localisation */
  setlocale (LC_ALL, "");
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  /* Set up signal handlers. */
  cancellable = g_cancellable_new ();
  signal_data.cancellable = g_object_ref (cancellable);

  sigint_id = g_unix_signal_add (SIGINT, signal_sigint_cb, &signal_data);
  sigterm_id = g_unix_signal_add (SIGTERM, signal_sigterm_cb, &signal_data);

  /* Handle command line parameters. */
  context = g_option_context_new (_("COMMAND [ARGS…]"));
  g_option_context_set_summary (context,
                                _("Query and access the SDK sensors and "
                                  "actuators service."));
  g_option_context_add_main_entries (context, entries, GETTEXT_PACKAGE);

  commands_help = list_available_commands (commands, G_N_ELEMENTS (commands));
  g_option_context_set_description (context, commands_help);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_autofree gchar *message;

      message = g_strdup_printf (_("Option parsing failed: %s"),
                                 error->message);
      g_printerr ("%s: %s\n", argv[0], message);

      return EXIT_INVALID_OPTIONS;
    }

  /* Connect to D-Bus. If no address was specified on the command line, use the
   * system bus. */
  if (bus_address == NULL)
    {
      bus_address = g_dbus_address_get_for_bus_sync (G_BUS_TYPE_SYSTEM,
                                                     cancellable, &error);
    }

  if (error != NULL)
    {
      g_autofree gchar *message;

      message = g_strdup_printf (_("D-Bus system bus unavailable: %s"),
                                 error->message);
      g_printerr ("%s: %s\n", argv[0], message);

      return EXIT_BUS_UNAVAILABLE;
    }

  connection = g_dbus_connection_new_for_address_sync (bus_address,
                                                       G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT |
                                                       G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION,
                                                       NULL  /* observer */,
                                                       cancellable, &error);

  if (connection == NULL)
    {
      g_autofree gchar *message;

      message = g_strdup_printf (_("D-Bus bus ‘%s’ unavailable: %s"),
                                 bus_address, error->message);
      g_printerr ("%s: %s\n", argv[0], message);

      return EXIT_BUS_UNAVAILABLE;
    }

  /* Set up a vehicle manager. */
  rsd_vehicle_manager_new_async (connection, cancellable,
                                 vehicle_manager_new_cb, &async_result);

  while (async_result == NULL)
    g_main_context_iteration (NULL, TRUE);

  manager = rsd_vehicle_manager_new_finish (async_result, &error);

  if (error != NULL)
    {
      g_autofree gchar *message;

      message = g_strdup_printf (_("Could not connect to vehicle manager: %s"),
                                 error->message);
      g_printerr ("%s: %s\n", argv[0], message);

      return EXIT_FAILED;
    }

  /* Work out which command to execute. */
  if (args == NULL || args[0] == NULL)
    {
      g_printerr ("%s: %s\n\n%s", argv[0], _("A command must be specified."),
                  commands_help);

      return EXIT_INVALID_OPTIONS;
    }

  for (i = 0; i < G_N_ELEMENTS (commands); i++)
    {
      if (g_strcmp0 (commands[i].name, args[0]) == 0)
        {
          /* Run the command asynchronously. The command will run asynchronously
           * in the main context iteration below, until it calls
           * g_task_return_*() on the command_task, at which point it will
           * return. */
          command_task = g_task_new (connection, cancellable, NULL, NULL);
          commands[i].func (manager, (const gchar * const *) args + 1,
                            command_task);
          break;
        }
    }

  if (i == G_N_ELEMENTS (commands))
    {
      g_autofree gchar *message = NULL;

      message = g_strdup_printf (_("Unrecognized command ‘%s’."), args[0]);
      g_printerr ("%s: %s\n\n%s", argv[0], message, commands_help);

      return EXIT_INVALID_OPTIONS;
    }

  /* Run the main loop until we are signalled or the command finishes. */
  while (!g_cancellable_is_cancelled (cancellable) &&
         !g_task_get_completed (command_task))
    g_main_context_iteration (NULL, TRUE);

  if (g_task_get_completed (command_task))
    {
      /* Handle errors from the command. */
      g_task_propagate_boolean (command_task, &error);

      if (error != NULL)
        {
          if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED) &&
              signal_data.signum != 0)
            raise (signal_data.signum);

          g_printerr ("%s: %s\n", argv[0], error->message);

          return EXIT_FAILED;
        }
    }

  return EXIT_OK;
}
