/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef RSD_VEHICLE_MANAGER_H
#define RSD_VEHICLE_MANAGER_H

#include <glib.h>
#include <glib-object.h>

#include <librhosydd/vehicle.h>

G_BEGIN_DECLS

#define RSD_TYPE_VEHICLE_MANAGER rsd_vehicle_manager_get_type ()
G_DECLARE_FINAL_TYPE (RsdVehicleManager, rsd_vehicle_manager, RSD,
                      VEHICLE_MANAGER, GObject)

void               rsd_vehicle_manager_new_async  (GDBusConnection     *connection,
                                                   GCancellable        *cancellable,
                                                   GAsyncReadyCallback  callback,
                                                   gpointer             user_data);
RsdVehicleManager *rsd_vehicle_manager_new_finish (GAsyncResult        *result,
                                                   GError             **error);

void               rsd_vehicle_manager_new_for_name_async  (GDBusConnection      *connection,
                                                            const gchar          *name,
                                                            const gchar          *object_path,
                                                            GCancellable         *cancellable,
                                                            GAsyncReadyCallback   callback,
                                                            gpointer              user_data);
RsdVehicleManager *rsd_vehicle_manager_new_for_name_finish (GAsyncResult         *result,
                                                            GError              **error);

GPtrArray  *rsd_vehicle_manager_list_vehicles (RsdVehicleManager *self);

RsdVehicle *rsd_vehicle_manager_get_vehicle   (RsdVehicleManager *self,
                                               const gchar       *vehicle_id);

const gchar *rsd_vehicle_manager_get_name     (RsdVehicleManager *self);

G_END_DECLS

#endif /* !RSD_VEHICLE_MANAGER_H */
