/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <locale.h>
#include <string.h>

#include "librhosydd/vehicle.h"


/* Test that invalid vehicle IDs are rejected and valid ones are accepted */
static void
test_vehicle_id_validation (void)
{
  const gchar *valid_ids[] = {
    "hello",
    "vehicle1",
    "vehicle0",
    "MY-CAR",
  };
  const gchar *invalid_ids[] = {
    NULL,
    "",
    " ",
    "hello!",
    "my car",
    "my_car",
    "0vehicle",
    "‽",
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (valid_ids); i++)
    {
      g_test_message ("Valid ID %" G_GSIZE_FORMAT ": %s", i, valid_ids[i]);
      g_assert_true (rsd_vehicle_id_is_valid (valid_ids[i]));
    }

  for (i = 0; i < G_N_ELEMENTS (invalid_ids); i++)
    {
      g_test_message ("Invalid ID %" G_GSIZE_FORMAT ": %s", i, invalid_ids[i]);
      g_assert_false (rsd_vehicle_id_is_valid (invalid_ids[i]));
    }
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/vehicle/id/validation", test_vehicle_id_validation);

  return g_test_run ();
}
