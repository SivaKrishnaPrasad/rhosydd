/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef DBUS_UTILITIES_H
#define DBUS_UTILITIES_H

#include <gio/gio.h>
#include <glib.h>


G_BEGIN_DECLS

/**
 * AsyncResultData:
 * @obj: source object of the asynchronous operation; %NULL until the operation
 *    is complete
 * @result: result of the asynchronous operation; %NULL until the operation is
 *    complete
 *
 * Structure to encapsulate waiting for and returning the results from an
 * asynchronous operation.
 *
 * Since: 0.2.0
 */
typedef struct
{
  GObject *obj;  /* owned */
  GAsyncResult *result;  /* owned */
} AsyncResultData;

AsyncResultData *async_result_data_new      (void);
void             async_result_data_free     (AsyncResultData *data);

void             async_result_data_callback (GObject         *obj,
                                             GAsyncResult    *result,
                                             gpointer         user_data);
void             async_result_data_yield    (AsyncResultData *data,
                                             GMainContext    *context);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (AsyncResultData, async_result_data_free);

/**
 * ExpectedMethodCall:
 * @sender: (nullable): D-Bus sender peer name, or %NULL if not running a
 *    message bus
 * @object_path: path of the object the method is being called on
 * @interface_name: name of the interface defining the method
 * @method_name: name of the method
 * @parameters: expected parameters for the method call, in #GVariant text
 *    format
 * @return_value: (nullable): value to return from the method call, in #GVariant
 *    text format, or %NULL to return @return_error
 * @return_error: (nullable): error to return from the method call, or %NULL to
 *    return @return_value
 *
 * Data describing a single expected D-Bus method call and its reply. Exactly
 * one of @return_value and @return_error must be specified.
 *
 * Since: 0.2.0
 */
typedef struct
{
  const gchar *sender;
  const gchar *object_path;
  const gchar *interface_name;
  const gchar *method_name;
  const gchar *parameters;  /* in GVariant string format */

  /* Mutually exclusive: */
  const gchar *return_value;  /* in GVariant string format */
  const GError return_error;
} ExpectedMethodCall;

/**
 * ExpectedGetProperty:
 * @sender: (nullable): D-Bus sender peer name, or %NULL if not running a
 *    message bus
 * @object_path: path of the object the property is being retrieved from
 * @interface_name: name of the interface defining the property
 * @property_name: name of the property
 * @return_value: (nullable): value to return for the property, in #GVariant
 *    text format, or %NULL to return @return_error
 * @return_error: (nullable): error to return for the property, or %NULL to
 *    return @return_value
 *
 * Data describing a single expected D-Bus `GetProperty` call and its returned
 * value. Exactly one of @return_value and @return_error must be specified.
 *
 * Since: 0.2.0
 */
typedef struct
{
  const gchar *sender;
  const gchar *object_path;
  const gchar *interface_name;
  const gchar *property_name;

  /* Mutually exclusive: */
  const gchar *return_value;  /* in GVariant string format */
  const GError return_error;
} ExpectedGetProperty;

/**
 * ExpectedSetProperty:
 * @sender: (nullable): D-Bus sender peer name, or %NULL if not running a
 *    message bus
 * @object_path: path of the object the property is being set on
 * @interface_name: name of the interface defining the property
 * @property_name: name of the property
 * @value: expected new value for the property, in #GVariant text format
 * @return_error: (nullable): error to return for the property, or %NULL to
 *    return success
 *
 * Data describing a single expected D-Bus `SetProperty` call and its success
 * value.
 *
 * Since: 0.2.0
 */
typedef struct
{
  const gchar *sender;
  const gchar *object_path;
  const gchar *interface_name;
  const gchar *property_name;
  const gchar *value;  /* in GVariant string format */
  const GError return_error;  /* unset iff property was set correctly */
} ExpectedSetProperty;

/**
 * EventType:
 * @EVENT_METHOD_CALL: a D-Bus method call
 * @EVENT_GET_PROPERTY: a D-Bus `GetProperty` method call
 * @EVENT_SET_PROPERTY: a D-Bus `SetProperty` method call
 *
 * Enumerated type categorising different possible expected D-Bus events.
 *
 * Since: 0.2.0
 */
typedef enum
{
  EVENT_METHOD_CALL,
  EVENT_GET_PROPERTY,
  EVENT_SET_PROPERTY,
} EventType;

/**
 * ExpectedEvent:
 * @type: type of the expected event
 * @method_call: an #ExpectedMethodCall event
 * @get_property: an #ExpectedGetProperty event
 * @set_property: an #ExpectedSetProperty event
 *
 * Tagged union type which combines all the different #EventType structures.
 *
 * Since: 0.2.0
 */
typedef struct
{
  EventType type;
  union {
    ExpectedMethodCall method_call;
    ExpectedGetProperty get_property;
    ExpectedSetProperty set_property;
  };
} ExpectedEvent;

/**
 * DBusFixture:
 * @server_connection: D-Bus connection from the server side
 * @client_connection: D-Bus connection from the client side
 * @queues: per-vehicle queues of expected D-Bus events to be seen during the
 *    test
 *
 * Test fixture designed to be used with g_test_add() which establishes a
 * peer-to-peer D-Bus connection (no message bus) and allows objects to be
 * exported on the @server_connection by the test harness, and consumed by the
 * code under test with the @client_connection.
 *
 * Tests succeed if all the expected D-Bus events (for each vehicle) are seen,
 * in order, through the course of the test; and no other events are seen. The
 * queues of #ExpectedEvent structures are maintained per-vehicle, and ordering
 * is not enforced across queues (only within queues).
 *
 * Since: 0.2.0
 */
typedef struct
{
  GDBusConnection *server_connection;  /* owned */
  GDBusConnection *client_connection;  /* owned */
  GHashTable/*<uint, owned GQueue<owned ExpectedEvent>>*/ *queues;  /* owned */
} DBusFixture;

void  dbus_fixture_setup             (DBusFixture               *fixture,
                                      gconstpointer              test_data);
void  dbus_fixture_teardown          (DBusFixture               *fixture,
                                      gconstpointer              test_data);

guint dbus_fixture_register_object   (DBusFixture               *fixture,
                                      const gchar               *object_path,
                                      const GDBusInterfaceInfo  *interface_info,
                                      const ExpectedEvent       *events,
                                      gsize                      n_events,
                                      GError                   **error);
void  dbus_fixture_unregister_object (DBusFixture               *fixture,
                                      guint                      id);

G_END_DECLS

#endif /* !DBUS_UTILITIES_H */
