/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <gio/gio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "libinternal/timestamped-pointer.h"
#include "librhosydd/subscription-manager.h"
#include "librhosydd/types.h"
#include "librhosydd/vehicle.h"
#include "librhosydd/attribute.h"
#include "vehicle.h"
#include "vehicle_builder.h"

static void speedo_vehicle_vehicle_init (RsdVehicleInterface *iface);

static void speedo_vehicle_dispose      (GObject             *object);

static void speedo_vehicle_get_property (GObject             *object,
                                         guint                property_id,
                                         GValue              *value,
                                         GParamSpec          *pspec);
static void speedo_vehicle_set_property (GObject             *object,
                                         guint                property_id,
                                         const GValue        *value,
                                         GParamSpec          *pspec);

static const gchar *speedo_vehicle_vehicle_get_id                     (RsdVehicle          *vehicle);

static void        speedo_vehicle_vehicle_get_attributes_async        (RsdVehicle          *vehicle,
                                                                       const gchar         *node_path,
                                                                       GCancellable        *cancellable,
                                                                       GAsyncReadyCallback  callback,
                                                                       gpointer             user_data);

static GPtrArray   *speedo_vehicle_vehicle_get_attributes_finish      (RsdVehicle          *vehicle,
                                                                       GAsyncResult        *result,
                                                                       RsdTimestampMicroseconds *current_time,
                                                                       GError             **error);

static void        speedo_vehicle_vehicle_get_metadata_async          (RsdVehicle          *vehicle,
                                                                       const gchar         *node_path,
                                                                       GCancellable        *cancellable,
                                                                       GAsyncReadyCallback  callback,
                                                                       gpointer             user_data);

static GPtrArray   *speedo_vehicle_vehicle_get_metadata_finish        (RsdVehicle          *vehicle,
                                                                       GAsyncResult        *result,
                                                                       RsdTimestampMicroseconds *current_time,
                                                                       GError             **error);

static void        speedo_vehicle_vehicle_update_subscriptions_async  (RsdVehicle          *vehicle,
                                                                       GPtrArray           *subscriptions,
                                                                       GPtrArray           *unsubscriptions,
                                                                       GCancellable        *cancellable,
                                                                       GAsyncReadyCallback  callback,
                                                                       gpointer             user_data);

static void        speedo_vehicle_vehicle_update_subscriptions_finish (RsdVehicle          *vehicle,
                                                                       GAsyncResult        *result,
                                                                       GError             **error);

static void        speedo_vehicle_vehicle_set_attributes_finish       (RsdVehicle          *vehicle,
                                                                       GAsyncResult        *result,
                                                                       GError             **error);

static void        speedo_vehicle_vehicle_set_attributes_async        (RsdVehicle          *self,
                                                                       GHashTable          *attribute_values,
                                                                       GCancellable        *cancellable,
                                                                       GAsyncReadyCallback  callback,
                                                                       gpointer             user_data);

static GPtrArray *attributes_info = NULL;

/**
 * SpeedoVehicle:
 *
 * An implementation of #RsdVehicle which simulates the continual acceleration
 * and deceleration of a car, in steps triggered by calls to
 * speedo_vehicle_update().
 *
 * This is meant as a demonstration, mock backend, **not for production use**.
 *
 * The vehicle has the constant ID of `speedo`. The acceleration and
 * deceleration vary constantly but deterministically with time.
 *
 * Since: 0.2.0
 */
struct _SpeedoVehicle
{
  GObject parent;

  gchar *id;  /* owned */
  RsdSubscriptionManager *subscriptions;  /* owned */

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-Acceleration */

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-VehicleSpeed */
  RsdAttribute speed;  /* m/h */

  /* One wheel per quadrant of the vehicle. */
};

typedef enum
{
  PROP_ID = 1,
} SpeedoVehicleProperty;

G_DEFINE_TYPE_WITH_CODE (SpeedoVehicle, speedo_vehicle, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (RSD_TYPE_VEHICLE,
                                                speedo_vehicle_vehicle_init))

static void
speedo_vehicle_class_init (SpeedoVehicleClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = speedo_vehicle_get_property;
  object_class->set_property = speedo_vehicle_set_property;
  object_class->dispose = speedo_vehicle_dispose;

  g_object_class_override_property (object_class, PROP_ID, "id");
}

static void
speedo_vehicle_vehicle_init (RsdVehicleInterface *iface)
{
  if(!attributes_info)
     attributes_info = build_vehicle();
  iface->get_id = speedo_vehicle_vehicle_get_id;
  iface->get_attributes_async = speedo_vehicle_vehicle_get_attributes_async;
  iface->get_attributes_finish = speedo_vehicle_vehicle_get_attributes_finish;
  iface->get_metadata_async = speedo_vehicle_vehicle_get_metadata_async;
  iface->get_metadata_finish = speedo_vehicle_vehicle_get_metadata_finish;
  iface->update_subscriptions_async = speedo_vehicle_vehicle_update_subscriptions_async;
  iface->update_subscriptions_finish = speedo_vehicle_vehicle_update_subscriptions_finish;
  iface->set_attributes_async = speedo_vehicle_vehicle_set_attributes_async;
  iface->set_attributes_finish = speedo_vehicle_vehicle_set_attributes_finish;
}

static gboolean
speedo_set_attribute (const gchar  *attribute_name,
                      GVariant     *value,
                      RsdVehicleError *error)
{
    guint i = 0;

    if(!attributes_info) 
       attributes_info = build_vehicle();

    if(attributes_info == NULL)
    {
        *error = RSD_VEHICLE_ERROR_UNKNOWN_ERROR; 
        return FALSE;
    }
    for( i = 0; i < attributes_info->len; i++ )
    {
        RsdAttributeInfo* info = attributes_info->pdata[i];
        if(!strcmp(info->metadata.name, attribute_name))
        {
            g_variant_unref(info->attribute.value);
            info->attribute.value = g_variant_ref(value);
            info->attribute.last_updated = g_get_monotonic_time();
            return TRUE;
        }
    }
    *error = RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE;
    return FALSE;   
}

static RsdAttributeInfo*
speedo_get_attribute_info (const gchar *attribute_name)
{
    guint i = 0;

  if(!attributes_info) 
     attributes_info = build_vehicle();

    if(attributes_info == NULL)
    {
        return NULL;
    }
    for( i = 0; i < attributes_info->len; i++ )
    {
        const RsdAttributeInfo* info = attributes_info->pdata[i];
        if(!strcmp(info->metadata.name, attribute_name))
        {
            RsdAttributeInfo* info1 = rsd_attribute_info_copy(info);
            return info1;
        }
    }
    return NULL;
}


static GVariant*
speedo_get_attribute_value (const gchar*attribute_name)
{
    guint i = 0;
    if(!attributes_info) 
       attributes_info = build_vehicle();

    if(attributes_info == NULL)
    {
        return NULL;
    }
    for( i = 0; i < attributes_info->len; i++ )
    {
        const RsdAttributeInfo* info = attributes_info->pdata[i];
        if(!strcmp(info->metadata.name, attribute_name))
        {
            return g_variant_ref(info->attribute.value);
        }
    }
    return NULL;
}  
                      
static void
speedo_vehicle_init (SpeedoVehicle *self)
{
  /* Create a subscription manager. */
  self->subscriptions = rsd_subscription_manager_new ();
  if(!attributes_info) 
     attributes_info = build_vehicle();
}

static void
speedo_vehicle_dispose (GObject *object)
{
  SpeedoVehicle *self = SPEEDO_VEHICLE (object);

  g_clear_object (&self->subscriptions);
  g_clear_pointer (&self->id, g_free);
  destroy_vehicle();
  /* Chain up to the parent class */
  G_OBJECT_CLASS (speedo_vehicle_parent_class)->dispose (object);
}

static void
speedo_vehicle_get_property (GObject    *object,
                             guint       property_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  SpeedoVehicle *self = SPEEDO_VEHICLE (object);

  switch ((SpeedoVehicleProperty) property_id)
    {
    case PROP_ID:
      g_value_set_string (value, self->id);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
speedo_vehicle_set_property (GObject      *object,
                             guint         property_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  SpeedoVehicle *self = SPEEDO_VEHICLE (object);

  switch ((SpeedoVehicleProperty) property_id)
    {
    case PROP_ID:
      /* Construct only. */
      g_assert (self->id == NULL);
      g_assert (rsd_vehicle_id_is_valid (g_value_get_string (value)));
      self->id = g_value_dup_string (value);
      break;
    default:
      g_assert_not_reached ();
    }
}

static const gchar *
speedo_vehicle_vehicle_get_id (RsdVehicle *vehicle)
{
  SpeedoVehicle *self = SPEEDO_VEHICLE (vehicle);

  return self->id;
}

static void
speedo_vehicle_vehicle_get_attributes_async (RsdVehicle          *vehicle,
                                             const gchar         *node_path,
                                             GCancellable        *cancellable,
                                             GAsyncReadyCallback  callback,
                                             gpointer             user_data)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;
  guint i;
  gboolean get_all = FALSE;
  g_autoptr (GError) error = NULL;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task, speedo_vehicle_vehicle_get_attributes_async);

  if(!attributes_info) 
     attributes_info = build_vehicle();

  if(attributes_info == NULL)
  {
      g_task_return_new_error (task, RSD_VEHICLE_ERROR,
                               RSD_VEHICLE_ERROR_UNKNOWN_ERROR,
                               _("Speedo Back-end internal error."));
      return;
  }

  if(!strlen(node_path))
     get_all = TRUE;
  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);

  for(i=0; i < attributes_info->len; i++)
  {
     const RsdAttributeInfo *attrInfo = attributes_info->pdata[i];
     if((get_all) || (!strncmp(node_path, attrInfo->metadata.name, strlen(node_path))))
     {
           g_ptr_array_add( attributes, 
                  rsd_attribute_info_copy(attrInfo)); 
     } 
  }     
  if(!attributes->len)
  {
      g_set_error (&error, RSD_VEHICLE_ERROR,
                   RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE,
                   _("Unknown Attribute or branch ‘%s’."),
                   node_path);
      g_task_return_error(task, g_steal_pointer(&error));
  }
  else
    timestamped_pointer_g_task_return (task, g_steal_pointer (&attributes),
                                      (GDestroyNotify) g_ptr_array_unref,
                                       g_get_monotonic_time());
}

static GPtrArray/*<owned RsdAttributeInfo>*/ *
speedo_vehicle_vehicle_get_attributes_finish (RsdVehicle                *vehicle,
                                              GAsyncResult              *result,
                                              RsdTimestampMicroseconds  *current_time,
                                              GError                   **error)
{
  return timestamped_pointer_g_task_propagate (G_TASK (result),
                                               current_time, error);
}

static void
speedo_vehicle_vehicle_get_metadata_async (RsdVehicle          *vehicle,
                                           const gchar         *node_path,
                                           GCancellable        *cancellable,
                                           GAsyncReadyCallback  callback,
                                           gpointer             user_data)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (GPtrArray/*<owned GVariant* for metadata*/) attributes = NULL;
  guint i;
  gboolean get_all = FALSE;
  g_autoptr (GError) error = NULL;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         speedo_vehicle_vehicle_get_metadata_async);
  if(!attributes_info) 
     attributes_info = build_vehicle();

  if(attributes_info == NULL)
  {
      g_task_return_new_error (task, RSD_VEHICLE_ERROR,
                               RSD_VEHICLE_ERROR_UNKNOWN_ERROR,
                               _("Speedo Back-end internal error."));
      return;
  }
  if(!strlen(node_path))
     get_all = TRUE;

  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_metadata_free);

  for(i = 0; i < attributes_info->len; i++)
  {
     const RsdAttributeInfo *attrInfo = attributes_info->pdata[i];
     if((get_all) || (!strncmp(node_path, attrInfo->metadata.name, strlen(node_path))))
          g_ptr_array_add(attributes, rsd_attribute_metadata_copy(&attrInfo->metadata));
  }
  if(!attributes->len)
  {
      g_set_error (&error, RSD_VEHICLE_ERROR,
                   RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE,
                   _("Unknown Attribute or branch ‘%s’."),
                    node_path);
      g_task_return_error(task, g_steal_pointer(&error));
  }
  else
     timestamped_pointer_g_task_return (task, g_steal_pointer (&attributes),
                                       (GDestroyNotify) g_ptr_array_unref,
                                        g_get_monotonic_time());
}

static GPtrArray/*<owned RsdAttributeInfo>*/ *
speedo_vehicle_vehicle_get_metadata_finish (RsdVehicle                *vehicle,
                                            GAsyncResult              *result,
                                            RsdTimestampMicroseconds  *current_time,
                                            GError                    **error)
{
  return timestamped_pointer_g_task_propagate (G_TASK (result),
                                               current_time, error);
}

static void
speedo_vehicle_vehicle_update_subscriptions_async (RsdVehicle          *vehicle,
                                                   GPtrArray           *subscriptions,
                                                   GPtrArray           *unsubscriptions,
                                                   GCancellable        *cancellable,
                                                   GAsyncReadyCallback  callback,
                                                   gpointer             user_data)
{
  SpeedoVehicle *self = SPEEDO_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         speedo_vehicle_vehicle_update_subscriptions_async);

  rsd_subscription_manager_update_subscriptions (self->subscriptions,
                                                 subscriptions,
                                                 unsubscriptions);

  g_task_return_boolean (task, TRUE);
}

static void
speedo_vehicle_vehicle_update_subscriptions_finish (RsdVehicle    *vehicle,
                                                    GAsyncResult  *result,
                                                    GError       **error)
{
  GTask *task = G_TASK (result);

  g_task_propagate_boolean (task, error);
}

static void
speedo_vehicle_vehicle_set_attributes_async (RsdVehicle            *vehicle,
                                             GHashTable            *attribute_values,
                                             GCancellable          *cancellable,
                                             GAsyncReadyCallback   callback,
                                             gpointer              user_data)
{
  GHashTableIter iter;
  SpeedoVehicle *self = SPEEDO_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  gboolean status = FALSE;
  RsdVehicleError error;
  const gchar *attribute_name;
  GVariant *value = NULL;
  
  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         speedo_vehicle_vehicle_set_attributes_async);

  if (!attributes_info) 
     attributes_info = build_vehicle();

  if (attributes_info == NULL)
    {
      g_task_return_new_error (task, RSD_VEHICLE_ERROR,
                               RSD_VEHICLE_ERROR_UNKNOWN_ERROR,
                               _("Speedo Back-end internal error."));
      return;
    }
  g_hash_table_iter_init (&iter, attribute_values);
  while( g_hash_table_iter_next (&iter,
            (gpointer *) &attribute_name, (gpointer *) &value))
    {
      status = speedo_set_attribute (attribute_name, value, &error);
      if (!status)
        {
          if (error == RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE)
             g_task_return_new_error (task,
                                      RSD_VEHICLE_ERROR,
                                      RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE,
                                      _("Speedo Back-end unknown attribute %s"),
                                      attribute_name);
          else 
             g_task_return_new_error (task, 
                                      RSD_VEHICLE_ERROR,
                                      RSD_VEHICLE_ERROR_UNKNOWN_ERROR,
                                      _("Speedo Back-end internal error."));  
        }
    }
  g_hash_table_remove_all(attribute_values);
  g_task_return_boolean (task, TRUE);
}

static void
speedo_vehicle_vehicle_set_attributes_finish (RsdVehicle    *vehicle,
                                              GAsyncResult  *result,
                                              GError        **error)
{
  GTask *task = G_TASK (result);
  g_task_propagate_boolean (task, error);
}


/**
 * speedo_vehicle_new:
 *
 * Create a new #SpeedoVehicle. Its ID is `speedo`.
 *
 * Returns: (transfer full): a new #SpeedoVehicle
 * Since: 0.2.0
 */
SpeedoVehicle *
speedo_vehicle_new (void)
{
  return g_object_new (SPEEDO_TYPE_VEHICLE,
                       "id", "speedo",
                       NULL);
}

/**
 * speedo_vehicle_update:
 * @self: a #SpeedoVehicle
 * @timestamp: simulation timestamp, in microseconds since some system-specific
 *     epoch
 *
 * Update the simulation state, which means updating the vehicle’s attributes
 * and emitting a #RsdVehicle::attributes-changed notification about them.
 *
 * The @timestamp should typically come from g_get_real_time(), but can come
 * from any other clock source in microseconds.
 *
 * Since: 0.2.0
 */
void
speedo_vehicle_update (SpeedoVehicle *self,
                       RsdTimestampMicroseconds   timestamp)
{
  gint32 old_acceleration_x, new_acceleration_x;  /* cm/s^2 */
  gint32 old_acceleration_y, new_acceleration_y;  /* cm/s^2 */
  RsdVehicleError error;
  gint64 new_acceleration_scalar;  /* cm/s^2 */
  guint16 old_speed, new_speed;  /* m/h */
  guint64 period;  /* µs */
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) changed_attributes = NULL;
  g_autoptr (GPtrArray/*<owned RsdAttributeMetadata>*/) invalidated_attributes = NULL;
  RsdTimestampMicroseconds last_update;
  g_autoptr(RsdAttributeInfo) acceleration_x = NULL;
  const gint64 acceleration_scale = 400;  /* cm/s^2 */

  acceleration_x = speedo_get_attribute_info("Signal.Acceleration.X");
  last_update = acceleration_x->attribute.last_updated;

  /* Ignore the update if we’re going backwards in time. */
  if (timestamp <= last_update)
     return;

  /* Work out the resultant of the old acceleration components. */
  old_acceleration_x = g_variant_get_int32 (acceleration_x->attribute.value);
  old_acceleration_y = g_variant_get_int32 (speedo_get_attribute_value("Signal.Acceleration.Y"));
 
  new_acceleration_x = sin (timestamp) * acceleration_scale;
  new_acceleration_y = cos (timestamp) * acceleration_scale;

  new_acceleration_scalar = sqrt (old_acceleration_x * old_acceleration_x +
                                  old_acceleration_y * old_acceleration_y);

  /* Update the speed given the time passed since the last update, and the
   * previous acceleration values. */

  period = timestamp - last_update;

  old_speed = g_variant_get_uint16 (speedo_get_attribute_value("Signal.Speed"));
  new_speed = old_speed +
              period / 1000000 *  /* convert from µs to s */
              new_acceleration_scalar / 100 * 60 * 60 * 60 * 60;  /* convert from cm/s^2 to m/h^2 */

  speedo_set_attribute ("Signal.Speed", g_variant_new_uint16 (new_speed),  &error);
  speedo_set_attribute ("Signal.Acceleration.X", g_variant_new_int32 (new_acceleration_x),  &error);
  speedo_set_attribute ("Signal.Acceleration.Y", g_variant_new_int32 (new_acceleration_y),  &error);

  /* Emit an update signal. */
  changed_attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);
  invalidated_attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_metadata_free);
  
  g_ptr_array_add (changed_attributes, speedo_get_attribute_info("Signal.Speed"));
  g_ptr_array_add (changed_attributes, speedo_get_attribute_info("Signal.Acceleration.X"));  
  g_ptr_array_add (changed_attributes, speedo_get_attribute_info("Signal.Acceleration.Y"));

  if (rsd_subscription_manager_is_subscribed (self->subscriptions,
                                              changed_attributes,
                                              invalidated_attributes,
                                              g_get_monotonic_time ()))

     g_signal_emit_by_name (self, "attributes-changed", g_get_monotonic_time(),
                            changed_attributes,
                            invalidated_attributes);
}

