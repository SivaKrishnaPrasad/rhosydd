/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib-unix.h>
#include <glib/gi18n-lib.h>
#include <gio/gio.h>
#include <locale.h>

#include "aggregate-vehicle.h"
#include "libcroesor/service.h"
#include "libcroesor/vehicle-service.h"
#include "libinternal/logging.h"
#include "librhosydd/proxy-vehicle.h"
#include "librhosydd/vehicle-manager.h"
#include "peer-manager.h"
#include "service.h"
#include "vehicle-multiplexer.h"


static void vdd_service_dispose      (GObject      *object);

static void vdd_service_startup_async  (CsrService          *service,
                                        GCancellable        *cancellable,
                                        GAsyncReadyCallback  callback,
                                        gpointer             user_data);
static void vdd_service_startup_finish (CsrService          *service,
                                        GAsyncResult        *result,
                                        GError             **error);
static void vdd_service_shutdown       (CsrService          *service);

/**
 * VddService:
 *
 * The core implementation of the vehicle device daemon (VDD), which exposes all
 * its D-Bus objects on the bus.
 *
 * Since: 0.2.0
 */
struct _VddService
{
  CsrService parent;

  guint name_owner_changed_id;
  CsrVehicleService *sdk_api;  /* owned */

  GCancellable *cancellable;  /* owned */

  /* Mapping from well-known D-Bus name to an #RsdVehicleManager which manages
   * the vehicles from that backend. */
  GHashTable/*<unowned utf8, owned RsdVehicleManager>*/ *backends;  /* owned */
  CsrVehicleManager/*<owned VddAggregateVehicle>*/ *vehicle_manager;  /* owned */

  gboolean startup_list_names_done;
  gboolean startup_list_activatable_names_done;
};

G_DEFINE_TYPE (VddService, vdd_service, CSR_TYPE_SERVICE)

static void
vdd_service_class_init (VddServiceClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  CsrServiceClass *service_class = (CsrServiceClass *) klass;

  object_class->dispose = vdd_service_dispose;

  service_class->startup_async = vdd_service_startup_async;
  service_class->startup_finish = vdd_service_startup_finish;
  service_class->shutdown = vdd_service_shutdown;
}

static void
vdd_service_init (VddService *self)
{
  /* We don’t have to copy the key because it’s guaranteed to be owned by the
   * #RsdVehicleManager which is the value. */
  self->backends = g_hash_table_new_full (g_str_hash, g_str_equal, NULL,
                                          g_object_unref);

  /* A cancellable to allow pending asynchronous operations to be cancelled when
   * shutdown() is called. */
  self->cancellable = g_cancellable_new ();
}

static void
vdd_service_dispose (GObject *object)
{
  VddService *self = VDD_SERVICE (object);

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  if (self->name_owner_changed_id != 0)
    {
      g_dbus_connection_signal_unsubscribe (csr_service_get_dbus_connection (CSR_SERVICE (self)),
                                            self->name_owner_changed_id);
      self->name_owner_changed_id = 0;
    }

  g_clear_object (&self->sdk_api);
  g_clear_object (&self->vehicle_manager);
  g_clear_pointer (&self->backends, g_hash_table_unref);

  /* Chain up to the parent class */
  G_OBJECT_CLASS (vdd_service_parent_class)->dispose (object);
}

static void add_backend    (VddService  *self,
                            const gchar *name);
static void remove_backend (VddService  *self,
                            const gchar *name);

static void
name_owner_changed_cb (GDBusConnection *connection,
                       const gchar     *sender_name,
                       const gchar     *object_path,
                       const gchar     *interface_name,
                       const gchar     *signal_name,
                       GVariant        *parameters,
                       gpointer         user_data)
{
  VddService *self = VDD_SERVICE (user_data);
  const gchar *name, *old_owner, *new_owner;

  g_assert (g_strcmp0 (sender_name, "org.freedesktop.DBus") == 0);
  g_assert (g_strcmp0 (object_path, "/org/freedesktop/DBus") == 0);
  g_assert (g_strcmp0 (interface_name, "org.freedesktop.DBus") == 0);
  g_assert (g_strcmp0 (signal_name, "NameOwnerChanged") == 0);

  g_variant_get (parameters, "(&s&s&s)", &name, &old_owner, &new_owner);

  /* We only care about name changes for backends. */
  DEBUG ("Owner of ‘%s’ changed from ‘%s’ to ‘%s’.",
         name, old_owner, new_owner);

  if (!g_str_has_prefix (name, "org.apertis.Rhosydd1.Backends."))
    return;

  /* If a backend has disappeared, remove all its vehicles. */
  if (g_strcmp0 (old_owner, "") != 0)
    remove_backend (self, name);

  /* If a backend has appeared, query it for its vehicles and start watching
   * it. */
  if (g_strcmp0 (new_owner, "") != 0)
    add_backend (self, name);
}

static void manager_vehicles_changed_cb (RsdVehicleManager               *manager,
                                         GPtrArray/*<owned RsdVehicle>*/ *added_vehicles,
                                         GPtrArray/*<owned RsdVehicle>*/ *removed_vehicles,
                                         gpointer                         user_data);
static void add_backend_cb              (GObject                         *source_object,
                                         GAsyncResult                    *result,
                                         gpointer                         user_data);

static void
add_backend (VddService  *self,
             const gchar *name)
{
  GDBusConnection *connection;
  const gchar *unique_name, *c;
  g_autofree gchar *object_path = NULL;

  /* Set up a vehicle manager for this backend. Any existing manager will be
   * cleared in the add_backend_cb() callback.
   *
   * The name is in the format `org.apertis.Rhosydd1.Backends.$unique_name`,
   * which must convert to an @object_path of the form
   * `/org/apertis/Rhosydd1/Backends/$unique_name`. $unique_name must be
   * alphanumeric (i.e. following the rules for a valid D-Bus name
   * component). */
  connection = csr_service_get_dbus_connection (CSR_SERVICE (self));

  g_assert (g_str_has_prefix (name, "org.apertis.Rhosydd1.Backends."));
  g_assert (g_dbus_is_name (name));
  unique_name = name + strlen ("org.apertis.Rhosydd1.Backends.");

  /* Validate the @unique_name is a valid name component.
   *
   * See: https://dbus.freedesktop.org/doc/dbus-specification.html#message-protocol-names-bus
   */
  for (c = unique_name; *c != '\0'; c++)
    {
      if (!(c == unique_name && (g_ascii_isalpha (*c) || *c == '_')) &&
          !(c != unique_name && (g_ascii_isalnum (*c) || *c == '_')))
        {
          WARNING ("Ignoring invalid backend name ‘%s’.", unique_name);
          return;
        }
    }

  object_path = g_strdup_printf ("/org/apertis/Rhosydd1/Backends/%s",
                                 unique_name);

  DEBUG ("Adding backend ‘%s’ with object path ‘%s’.", name, object_path);

  rsd_vehicle_manager_new_for_name_async (connection, name,
                                          object_path,
                                          self->cancellable,
                                          add_backend_cb,
                                          self);
}

static void
add_backend_cb (GObject      *source_object,
                GAsyncResult *result,
                gpointer      user_data)
{
  VddService *self = VDD_SERVICE (user_data);
  g_autoptr (RsdVehicleManager) manager = NULL;
  RsdVehicleManager *old_manager;
  g_autoptr (GError) error = NULL;
  g_autoptr (GPtrArray/*<owned RsdVehicle>*/) added_vehicles = NULL;
  g_autoptr (GPtrArray/*<owned RsdVehicle>*/) removed_vehicles = NULL;
  const gchar *name;

  manager = rsd_vehicle_manager_new_for_name_finish (result, &error);
  name = rsd_vehicle_manager_get_name (RSD_VEHICLE_MANAGER (source_object));

  if (error != NULL)
    {
      WARNING ("Error setting up vehicle manager for backend ‘%s’: %s",
                 name, error->message);
      return;
    }

  DEBUG ("Constructed manager for new backend ‘%s’.", name);

  /* Do we already have a vehicle manager for this backend? If so, remove all
   * its vehicles. */
  old_manager = g_hash_table_lookup (self->backends, (gpointer) name);

  if (old_manager != NULL)
    {
      DEBUG ("Removing old manager for new backend ‘%s’.", name);

      removed_vehicles = rsd_vehicle_manager_list_vehicles (old_manager);
      g_signal_handlers_disconnect_by_data (old_manager, self);
    }

  /* Add the #RsdVehicleManager to our list of backends. */
  g_hash_table_replace (self->backends, (gpointer) name,
                        g_object_ref (manager));
  g_signal_connect (manager, "vehicles-changed",
                    (GCallback) manager_vehicles_changed_cb, self);

  /* For each of the vehicles exposed by the backend, add them to an aggregate
   * vehicle. */
  added_vehicles = rsd_vehicle_manager_list_vehicles (manager);
  vdd_vehicle_multiplexer_update_vehicles (added_vehicles, removed_vehicles,
                                           self->vehicle_manager);
}

static void
remove_backend (VddService  *self,
                const gchar *name)
{
  RsdVehicleManager *manager;
  g_autoptr (GPtrArray/*<owned RsdVehicle>*/) removed_vehicles = NULL;

  DEBUG ("Removing backend ‘%s’.", name);

  manager = g_hash_table_lookup (self->backends, (gpointer) name);
  if (manager == NULL)
    {
      DEBUG ("Backend already removed.");
      return;
    }

  removed_vehicles = rsd_vehicle_manager_list_vehicles (manager);
  DEBUG ("Backend ‘%s’ has %u vehicles to remove.", name,
         removed_vehicles->len);

  g_signal_handlers_disconnect_by_data (manager, self);
  g_hash_table_remove (self->backends, (gpointer) name);

  /* Update the aggregated vehicles in the vehicle manager. */
  vdd_vehicle_multiplexer_update_vehicles (NULL, removed_vehicles,
                                           self->vehicle_manager);
}

static void
manager_vehicles_changed_cb (RsdVehicleManager               *manager,
                             GPtrArray/*<owned RsdVehicle>*/ *added_vehicles,
                             GPtrArray/*<owned RsdVehicle>*/ *removed_vehicles,
                             gpointer                         user_data)
{
  VddService *self = VDD_SERVICE (user_data);

  vdd_vehicle_multiplexer_update_vehicles (added_vehicles, removed_vehicles,
                                           self->vehicle_manager);
}

static void vdd_service_startup_async_cb1  (GObject      *obj,
                                            GAsyncResult *result,
                                            gpointer      user_data);
static void vdd_service_startup_async_cb2a (GObject      *obj,
                                            GAsyncResult *result,
                                            gpointer      user_data);
static void vdd_service_startup_async_cb2b (GObject      *obj,
                                            GAsyncResult *result,
                                            gpointer      user_data);
static void backend_started_cb             (GObject      *source_object,
                                            GAsyncResult *result,
                                            gpointer      user_data);

static void
vdd_service_startup_async (CsrService          *service,
                           GCancellable        *cancellable,
                           GAsyncReadyCallback  callback,
                           gpointer             user_data)
{
  g_autoptr (GTask) task = NULL;

  task = g_task_new (service, cancellable, callback, user_data);
  g_task_set_source_tag (task, vdd_service_startup_async);

  /* To avoid having to carry a lot of state to the callback, do the only
   * asynchronous operation first — set up the peer manager. */
  vdd_peer_manager_new_async (cancellable, vdd_service_startup_async_cb1,
                              g_steal_pointer (&task));
}

static void
vdd_service_startup_async_cb1 (GObject      *obj,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  VddService *self;
  g_autoptr (GTask) task = NULL;
  g_autoptr (GError) child_error = NULL;
  g_autoptr (CsrPeerManager) peer_manager = NULL;
  g_autoptr (CsrSubscriptionManager) subscription_manager = NULL;
  GDBusConnection *connection;
  GCancellable *cancellable;

  task = G_TASK (user_data);
  cancellable = g_task_get_cancellable (task);
  self = g_task_get_source_object (task);
  connection = csr_service_get_dbus_connection (CSR_SERVICE (self));

  peer_manager = CSR_PEER_MANAGER (vdd_peer_manager_new_finish (result,
                                                                &child_error));

  if (child_error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&child_error));
      return;
    }

  /* Shared state is stored in the various managers. */
  self->vehicle_manager = csr_vehicle_manager_new ();
  subscription_manager = csr_subscription_manager_new ();

  /* Create the SDK API on D-Bus. */
  self->sdk_api = csr_vehicle_service_new (connection, "/org/apertis/Rhosydd1",
                                           self->vehicle_manager, peer_manager,
                                           subscription_manager);

  if (!csr_vehicle_service_register (self->sdk_api, &child_error))
    {
      g_task_return_new_error (task, CSR_SERVICE_ERROR,
                               CSR_SERVICE_ERROR_NAME_UNAVAILABLE,
                               _("Error registering objects on bus: %s"),
                               child_error->message);
      return;
    }

  /* Listen for well-known name changes so we can query new backends when they
   * appear. */
  self->name_owner_changed_id = g_dbus_connection_signal_subscribe (connection,
                                                                    "org.freedesktop.DBus",
                                                                    "org.freedesktop.DBus",
                                                                    "NameOwnerChanged",
                                                                    "/org/freedesktop/DBus",
                                                                    "org.apertis.Rhosydd1.Backends",
                                                                    G_DBUS_SIGNAL_FLAGS_MATCH_ARG0_NAMESPACE,
                                                                    name_owner_changed_cb,
                                                                    self,
                                                                    NULL);

  /* List all the existing names and connect to those. */
  g_dbus_connection_call (connection, "org.freedesktop.DBus", "/",
                          "org.freedesktop.DBus", "ListNames", NULL,
                          G_VARIANT_TYPE ("(as)"), G_DBUS_CALL_FLAGS_NONE, -1,
                          cancellable, vdd_service_startup_async_cb2a,
                          g_object_ref (task));

  /* List the activatable services and find all the backends we can activate.
   * The overall startup will finish once both of these calls have returned (but
   * will not wait for the resulting services to be started). */
  g_dbus_connection_call (connection, "org.freedesktop.DBus", "/",
                          "org.freedesktop.DBus", "ListActivatableNames", NULL,
                          G_VARIANT_TYPE ("(as)"), G_DBUS_CALL_FLAGS_NONE, -1,
                          cancellable, vdd_service_startup_async_cb2b,
                          g_object_ref (task));
}

static void
vdd_service_startup_async_cb2a (GObject      *obj,
                                GAsyncResult *result,
                                gpointer      user_data)
{
  VddService *self;
  g_autoptr (GTask) task = NULL;
  g_autoptr (GError) child_error = NULL;
  g_autoptr (GVariant) tuple = NULL;
  g_autoptr (GVariant) names = NULL;
  GDBusConnection *connection = G_DBUS_CONNECTION (obj);
  GVariantIter iter;
  const gchar *name;

  task = G_TASK (user_data);
  self = g_task_get_source_object (task);

  tuple = g_dbus_connection_call_finish (connection, result, &child_error);

  if (child_error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&child_error));
      return;
    }

  /* For each of the namess, work out whether it’s one of our
   * backends, and then add it. */
  names = g_variant_get_child_value (tuple, 0);
  g_variant_iter_init (&iter, names);
  while (g_variant_iter_next (&iter, "&s", &name))
    {
      DEBUG ("Found existing name: %s", name);

      if (!g_str_has_prefix (name, "org.apertis.Rhosydd1.Backends."))
        continue;

      /* Spawn the backend asynchronously; don’t block on this. */
      DEBUG ("Adding backend ‘%s’.", name);
      add_backend (self, name);
    }

  /* Finished? */
  self->startup_list_names_done = TRUE;

  if (self->startup_list_names_done &&
      self->startup_list_activatable_names_done)
    g_task_return_boolean (task, TRUE);
}

static void
vdd_service_startup_async_cb2b (GObject      *obj,
                                GAsyncResult *result,
                                gpointer      user_data)
{
  VddService *self;
  g_autoptr (GTask) task = NULL;
  g_autoptr (GError) child_error = NULL;
  g_autoptr (GVariant) tuple = NULL;
  g_autoptr (GVariant) activatable_names = NULL;
  GDBusConnection *connection = G_DBUS_CONNECTION (obj);
  GVariantIter iter;
  GCancellable *cancellable;
  const gchar *activatable_name;

  task = G_TASK (user_data);
  cancellable = g_task_get_cancellable (task);
  self = g_task_get_source_object (task);

  tuple = g_dbus_connection_call_finish (connection, result, &child_error);

  if (child_error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&child_error));
      return;
    }

  /* For each of the activatable names, work out whether it’s one of our
   * backends, and then load it. */
  activatable_names = g_variant_get_child_value (tuple, 0);
  g_variant_iter_init (&iter, activatable_names);
  while (g_variant_iter_next (&iter, "&s", &activatable_name))
    {
      DEBUG ("Found activatable name: %s", activatable_name);

      if (!g_str_has_prefix (activatable_name,
                             "org.apertis.Rhosydd1.Backends."))
        continue;

      /* Spawn the backend asynchronously; don’t block on this. */
      DEBUG ("Starting service ‘%s’.", activatable_name);
      g_dbus_connection_call (connection, "org.freedesktop.DBus", "/",
                              "org.freedesktop.DBus", "StartServiceByName",
                              g_variant_new ("(su)", activatable_name,
                                             (guint32) 0),
                              G_VARIANT_TYPE ("(u)"), G_DBUS_CALL_FLAGS_NONE,
                              -1, cancellable, backend_started_cb,
                              g_strdup (activatable_name));
    }

  /* Finished? */
  self->startup_list_activatable_names_done = TRUE;

  if (self->startup_list_names_done &&
      self->startup_list_activatable_names_done)
    g_task_return_boolean (task, TRUE);
}

static void
backend_started_cb (GObject      *source_object,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  GDBusConnection *connection = G_DBUS_CONNECTION (source_object);
  g_autofree gchar *activatable_name = user_data;
  g_autoptr (GError) child_error = NULL;
  g_autoptr (GVariant) tuple = NULL;
  guint32 status;

  /* Reference:
   * https://dbus.freedesktop.org/doc/dbus-specification.html#bus-messages-start-service-by-name */
  typedef enum {
    DBUS_START_REPLY_SUCCESS = 1,
    DBUS_START_REPLY_ALREADY_RUNNING = 2,
  } DBusStartServiceReply;

  /* Give some debug output about the results of starting the backend, but we
   * don’t have to do anything else or update any state, as we will detect the
   * daemon’s well-known name when it appears. */
  tuple = g_dbus_connection_call_finish (connection, result, &child_error);

  if (child_error != NULL)
    {
      DEBUG ("Error spawning backend ‘%s’: %s",
             activatable_name, child_error->message);
      return;
    }

  g_variant_get (tuple, "(u)", &status);

  switch ((DBusStartServiceReply) status)
    {
    case DBUS_START_REPLY_SUCCESS:
      DEBUG ("Successfully started backend ‘%s’.", activatable_name);
      break;
    case DBUS_START_REPLY_ALREADY_RUNNING:
      DEBUG ("Backend ‘%s’ already running.", activatable_name);
      break;
    default:
      DEBUG ("Unknown status %u when starting backend ‘%s’.",
             status, activatable_name);
      break;
    }
}

static void
vdd_service_startup_finish (CsrService    *service,
                            GAsyncResult  *result,
                            GError       **error)
{
  g_task_propagate_boolean (G_TASK (result), error);
}

static void
vdd_service_shutdown (CsrService *service)
{
  VddService *self = VDD_SERVICE (service);

  g_cancellable_cancel (self->cancellable);

  if (self->name_owner_changed_id != 0)
    g_dbus_connection_signal_unsubscribe (csr_service_get_dbus_connection (CSR_SERVICE (self)),
                                          self->name_owner_changed_id);
  self->name_owner_changed_id = 0;

  csr_vehicle_service_unregister (self->sdk_api);

  g_hash_table_remove_all (self->backends);
}

/**
 * vdd_service_new:
 *
 * Create a new #VddService.
 *
 * Returns: (transfer full): a new #VddService
 * Since: 0.2.0
 */
VddService *
vdd_service_new (void)
{
  return g_object_new (VDD_TYPE_SERVICE,
                       "bus-type", G_BUS_TYPE_SYSTEM,
                       "service-id", "org.apertis.Rhosydd1",
                       "translation-domain", GETTEXT_PACKAGE,
                       "parameter-string", _("— aggregate sensor inputs"),
                       "summary", _("Aggregate sensor inputs from a variety of "
                                    "backends, and permit access to control "
                                    "actuators in the vehicle."),
                       NULL);
}
