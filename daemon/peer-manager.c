/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <polkit/polkit.h>

#include <libcroesor/peer-manager.h>
#include <libinternal/logging.h>

#include "peer-manager.h"


static void            vdd_peer_manager_async_initable_init        (GAsyncInitableIface      *iface);
static void            vdd_peer_manager_peer_manager_init          (CsrPeerManagerInterface  *iface);
static void            vdd_peer_manager_dispose                    (GObject                  *object);

static void            vdd_peer_manager_init_async                 (GAsyncInitable           *initable,
                                                                    int                       io_priority,
                                                                    GCancellable             *cancellable,
                                                                    GAsyncReadyCallback       callback,
                                                                    gpointer                  user_data);
static gboolean        vdd_peer_manager_init_finish                (GAsyncInitable           *initable,
                                                                    GAsyncResult             *result,
                                                                    GError                  **error);

static void            vdd_peer_manager_ensure_peer_info_async     (CsrPeerManager           *peer_manager,
                                                                    const gchar              *sender,
                                                                    GDBusConnection          *connection,
                                                                    GCancellable             *cancellable,
                                                                    GAsyncReadyCallback       callback,
                                                                    gpointer                  user_data);
/* TODO function returning CbyProcessInfo for
 * vdd_peer_manager_ensure_peer_info_finish need to
 * update with latest implementation
 */

static void *          vdd_peer_manager_ensure_peer_info_finish    (CsrPeerManager           *peer_manager,
                                                                    GAsyncResult             *result,
                                                                    GError                  **error);
static void            vdd_peer_manager_check_authorization_async  (CsrPeerManager           *peer_manager,
                                                                    const gchar              *sender,
                                                                    GDBusConnection          *connection,
                                                                    const gchar              *action_id,
                                                                    PolkitDetails            *details,
                                                                    GCancellable             *cancellable,
                                                                    GAsyncReadyCallback       callback,
                                                                    gpointer                  user_data);
static void            vdd_peer_manager_check_authorization_finish (CsrPeerManager           *peer_manager,
                                                                    GAsyncResult             *result,
                                                                    GError                  **error);

static void            vdd_peer_manager_get_property               (GObject                  *object,
                                                                    guint                     property_id,
                                                                    GValue                   *value,
                                                                    GParamSpec               *pspec);
static void            vdd_peer_manager_set_property               (GObject                  *object,
                                                                    guint                     property_id,
                                                                    const GValue             *value,
                                                                    GParamSpec               *pspec)
                                                                   G_GNUC_NORETURN;

static void            authority_changed_cb                        (PolkitAuthority          *authority,
                                                                    gpointer                  user_data);

/* Structure for values in the peers table. */
typedef struct
{
  /* TODO
   * CbyProcessInfo *process_info;
   **/
  guint watch_id;
} PeerInfo;

static void
peer_info_free (PeerInfo *info)
{
  /*TODO
   * g_object_unref (info->process_info);
   **/
  if (info->watch_id != 0)
    g_bus_unwatch_name (info->watch_id);

  g_free (info);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (PeerInfo, peer_info_free)

/**
 * VddPeerManager:
 *
 * A manager object which manages the set of known D-Bus peer processes, caching
 * information about them and allowing polkit authorisation checks against them
 * while avoiding time-of-check-to-time-of-use vulnerabilities.
 *
 * Since: 0.4.0
 */
struct _VddPeerManager
{
  GObject parent;

  GHashTable/*<owned utf8, owned PeerInfo>*/ *peers;  /* owned */
  PolkitAuthority *authority;  /* owned */
  GCancellable *cancellable;  /* owned */
  GError *init_error;  /* nullable; owned */
  gboolean initialising;
};

typedef enum
{
  /* Overridden properties: */
  PROP_PEERS = 1,
} VddPeerManagerProperty;

G_DEFINE_TYPE_WITH_CODE (VddPeerManager, vdd_peer_manager, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE,
                                                vdd_peer_manager_async_initable_init)
                         G_IMPLEMENT_INTERFACE (CSR_TYPE_PEER_MANAGER,
                                                vdd_peer_manager_peer_manager_init))

static void
vdd_peer_manager_class_init (VddPeerManagerClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->dispose = vdd_peer_manager_dispose;
  object_class->get_property = vdd_peer_manager_get_property;
  object_class->set_property = vdd_peer_manager_set_property;

  g_object_class_override_property (object_class, PROP_PEERS, "peers");
}

static void
vdd_peer_manager_init (VddPeerManager *self)
{
  self->cancellable = g_cancellable_new ();
  self->peers = g_hash_table_new_full (g_str_hash, g_str_equal,
                                       g_free, (GDestroyNotify) peer_info_free);
}

static void
vdd_peer_manager_async_initable_init (GAsyncInitableIface *iface)
{
  iface->init_async = vdd_peer_manager_init_async;
  iface->init_finish = vdd_peer_manager_init_finish;
}

static void
vdd_peer_manager_peer_manager_init (CsrPeerManagerInterface *iface)
{
  iface->ensure_peer_info_async = vdd_peer_manager_ensure_peer_info_async;
  iface->ensure_peer_info_finish = vdd_peer_manager_ensure_peer_info_finish;
  iface->check_authorization_async = vdd_peer_manager_check_authorization_async;
  iface->check_authorization_finish = vdd_peer_manager_check_authorization_finish;
}

static void get_authority_cb (GObject      *obj,
                              GAsyncResult *result,
                              gpointer      user_data);

static void
vdd_peer_manager_init_async (GAsyncInitable      *initable,
                             int                  io_priority,
                             GCancellable        *cancellable,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data)
{
  VddPeerManager *self = VDD_PEER_MANAGER (initable);
  g_autoptr (GTask) task = NULL;

  /* FIXME: Technically this isn't parallel-init-safe, but that doesn't
   * really matter. Catering for that case means storing the GTask and yielding
   * the second call on it completing. Since there is no constructor for
   * #VddPeerManager which caches singletons, this will not happen in practice
   * at the moment. */
  g_assert (!self->initialising);

  task = g_task_new (initable, cancellable, callback, user_data);
  g_task_set_source_tag (task, vdd_peer_manager_init_async);

  if (self->init_error != NULL)
    g_task_return_error (task, g_error_copy (self->init_error));
  else if (self->authority != NULL)
    g_task_return_boolean (task, TRUE);
  else
    {
      self->initialising = TRUE;
      polkit_authority_get_async (cancellable, get_authority_cb,
                                  g_steal_pointer (&task));
    }
}

static void
get_authority_cb (GObject      *obj,
                  GAsyncResult *result,
                  gpointer      user_data)
{
  VddPeerManager *self;
  g_autoptr (GTask) task = G_TASK (user_data);
  g_autoptr (GError) error = NULL;

  self = g_task_get_source_object (task);
  g_assert (self->init_error == NULL);

  self->authority = polkit_authority_get_finish (result, &error);

  g_assert (self->initialising);
  self->initialising = FALSE;

  if (error != NULL)
    {
      self->init_error = g_error_copy (error);
      g_task_return_error (task, g_steal_pointer (&error));
    }
  else
    {
      g_signal_connect (self->authority, "changed",
                        (GCallback) authority_changed_cb, self);
      g_task_return_boolean (task, TRUE);
    }
}

static gboolean
vdd_peer_manager_init_finish (GAsyncInitable  *initable,
                              GAsyncResult    *result,
                              GError         **error)
{
  GTask *task = G_TASK (result);

  return g_task_propagate_boolean (task, error);
}

static void
vdd_peer_manager_dispose (GObject *object)
{
  VddPeerManager *self = VDD_PEER_MANAGER (object);

  /* Bus watches will be automatically removed when the #PeerInfo structs are
   * freed. */
  g_clear_pointer (&self->peers, g_hash_table_unref);

  g_signal_handlers_disconnect_by_data (self->authority, self);
  g_clear_object (&self->authority);

  g_clear_object (&self->cancellable);
  g_clear_error (&self->init_error);

  /* Chain up to the parent class */
  G_OBJECT_CLASS (vdd_peer_manager_parent_class)->dispose (object);
}

static void
vdd_peer_manager_get_property (GObject    *object,
                               guint       property_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  VddPeerManager *self = VDD_PEER_MANAGER (object);

  switch ((VddPeerManagerProperty) property_id)
    {
    case PROP_PEERS:
      {
        g_autofree const gchar **peers = NULL;

        peers = (const gchar **) g_hash_table_get_keys_as_array (self->peers,
                                                                 NULL);
        g_value_set_boxed (value, peers);
        break;
      }
    default:
      g_assert_not_reached ();
    }
}

static void
vdd_peer_manager_set_property (GObject      *object,
                               guint         property_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  switch ((VddPeerManagerProperty) property_id)
    {
    case PROP_PEERS:
      /* Read only. */
      g_assert_not_reached ();
      break;
    default:
      g_assert_not_reached ();
    }
}

/* Called when the actions or authorisations in the #PolkitAuthority change. */
static void
authority_changed_cb (PolkitAuthority *authority,
                      gpointer         user_data)
{
  /* FIXME: Clear any cached authorisations, once we acquire a cache. */
}

/**
 * vdd_peer_manager_new_async:
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: callback to invoke on completion
 * @user_data: user data to pass to @callback
 *
 * Create a new #VddPeerManager with no initial peers, and asynchronously
 * set it up. This is an asynchronous process which might fail; object
 * instantiation must be finished (or the error returned) by calling
 * vdd_peer_manager_new_finish().
 *
 * Since: 0.4.0
 */
void
vdd_peer_manager_new_async (GCancellable        *cancellable,
                            GAsyncReadyCallback  callback,
                            gpointer             user_data)
{
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  g_async_initable_new_async (VDD_TYPE_PEER_MANAGER, G_PRIORITY_DEFAULT,
                              cancellable, callback, user_data, NULL);
}

/**
 * vdd_peer_manager_new_finish:
 * @result: asynchronous operation result
 * @error: return location for a #GError
 *
 * Finish initialising a #VddPeerManager. See vdd_peer_manager_new_async().
 *
 * Returns: (transfer full): initialised #VddPeerManager, or %NULL on error
 * Since: 0.4.0
 */
VddPeerManager *
vdd_peer_manager_new_finish (GAsyncResult    *result,
                             GError         **error)
{
  g_autoptr (GObject) source_object = NULL;

  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  source_object = g_async_result_get_source_object (result);
  return VDD_PEER_MANAGER (g_async_initable_new_finish (G_ASYNC_INITABLE (source_object),
                                                        result, error));
}

static void ensure_peer_info_cb (GObject      *object,
                                 GAsyncResult *result,
                                 gpointer      user_data);
static void peer_vanished_cb    (GDBusConnection *connection,
                                 const gchar     *name,
                                 gpointer         user_data);

typedef struct
{
  gchar *sender;  /* owned */
  GDBusConnection *connection;  /* owned */
} EnsurePeerInfoData;

static void
ensure_peer_info_data_free (EnsurePeerInfoData *data)
{
  g_clear_pointer (&data->sender, g_free);
  g_clear_object (&data->connection);

  g_free (data);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (EnsurePeerInfoData, ensure_peer_info_data_free)

static void
vdd_peer_manager_ensure_peer_info_async (CsrPeerManager      *peer_manager,
                                         const gchar         *sender,
                                         GDBusConnection     *connection,
                                         GCancellable        *cancellable,
                                         GAsyncReadyCallback  callback,
                                         gpointer             user_data)
{
  VddPeerManager *self = VDD_PEER_MANAGER (peer_manager);
  g_autoptr (GTask) task = NULL;
  PeerInfo *peer_info;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, vdd_peer_manager_ensure_peer_info_async);

  peer_info = g_hash_table_lookup (self->peers, sender);

  if (peer_info != NULL)
    {
      /*TODO
       * g_task_return_pointer (task, g_object_ref (peer_info->process_info),g_object_unref);
       **/
      return;
    }
  else
    {
      g_autoptr (EnsurePeerInfoData) data = NULL;

      data = g_new0 (EnsurePeerInfoData, 1);
      data->sender = g_strdup (sender);
      data->connection = g_object_ref (connection);

      g_task_set_task_data (task, g_steal_pointer (&data),
                            (GDestroyNotify) ensure_peer_info_data_free);

      /* TODO
      cby_process_info_new_for_dbus_sender_async (sender, connection,
                                                  cancellable,
                                                  ensure_peer_info_cb,
                                                  g_steal_pointer (&task));
      */
    }
}

static void
ensure_peer_info_cb (GObject      *object,
                     GAsyncResult *result,
                     gpointer      user_data)
{
  VddPeerManager *self;
  g_autoptr (GTask) task = G_TASK (user_data);
  EnsurePeerInfoData *data;
  /* TODO
   * g_autoptr (CbyProcessInfo) process_info = NULL;
   */
  g_autoptr (PeerInfo) peer_info = NULL;
  GError *error = NULL;

  self = VDD_PEER_MANAGER (g_task_get_source_object (task));
  /* TODO
   * process_info = cby_process_info_new_for_dbus_sender_finish (result, &error);
   */
  data = g_task_get_task_data (task);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  /* Add to the table of watched peers. We can guarantee that the
   * peer_vanished_cb() callback will not be invoked from within our call to
   * g_bus_watch_name_on_connection(), so there’s no need to worry about this
   * PeerInfo being destroyed part-way through creation.
   *
   * If there were two concurrent calls to
   * vdd_peer_manager_ensure_peer_info_async() for the same peer, the #PeerInfo
   * for the second one will replace that from the first, which is fine.
   *
   * FIXME: Add a unit test for this. */
  peer_info = g_new0 (PeerInfo, 1);
  /*TODO
   * peer_info->process_info = g_object_ref (process_info);
   **/
  peer_info->watch_id = g_bus_watch_name_on_connection (data->connection,
                                                        data->sender,
                                                        G_BUS_NAME_WATCHER_FLAGS_NONE,
                                                        NULL,
                                                        peer_vanished_cb, self,
                                                        NULL);
  //TODO
  #if 0
  g_hash_table_insert (self->peers, g_strdup (data->sender),
                       g_steal_pointer (&peer_info));
  g_object_notify (G_OBJECT (self), "peers");

  g_task_return_pointer (task, g_object_ref (process_info), g_object_unref);
  #endif
}

static void
peer_vanished_cb (GDBusConnection *connection,
                  const gchar     *name,
                  gpointer         user_data)
{
  VddPeerManager *self = VDD_PEER_MANAGER (user_data);
  g_autofree gchar *name_copy = g_strdup (name);

  /* Signal the vanishing, if this was a peer we were tracking. Keep a copy of
   * the name for the signal emission, as it may be freed by GIO when we unwatch
   * the name as part of the cleanup triggered by g_hash_table_remove(). */
  if (g_hash_table_remove (self->peers, name))
    {
      DEBUG ("Peer ‘%s’ vanished.", name_copy);

      g_signal_emit_by_name (self, "peer-vanished", name_copy);
      g_object_notify (G_OBJECT (self), "peers");
    }
}

/* TODO function returning CbyProcessInfo for
 * vdd_peer_manager_ensure_peer_info_finish need to
 * update with latest implementation
 */
static void *
vdd_peer_manager_ensure_peer_info_finish (CsrPeerManager  *peer_manager,
                                          GAsyncResult    *result,
                                          GError         **error)
{
  GTask *task = G_TASK (result);

  g_return_val_if_fail (g_task_is_valid (result, peer_manager), NULL);

  /* TODO
  return g_task_propagate_pointer (task, error);
  */
}

typedef struct
{
  gchar *sender;  /* owned */
  GDBusConnection *connection;  /* owned */
  gchar *action_id;  /* owned */
  PolkitDetails *details;  /* owned */
} AuthorizationCheckData;

static void
authorization_check_data_free (AuthorizationCheckData *data)
{
  g_clear_pointer (&data->sender, g_free);
  g_clear_object (&data->connection);
  g_clear_pointer (&data->action_id, g_free);
  g_clear_object (&data->details);

  g_free (data);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (AuthorizationCheckData,
                               authorization_check_data_free)

static void check_authorization_cb1 (GObject      *obj,
                                     GAsyncResult *result,
                                     gpointer      user_data);
static void check_authorization_cb2 (GObject      *obj,
                                     GAsyncResult *result,
                                     gpointer      user_data);

static void
vdd_peer_manager_check_authorization_async (CsrPeerManager      *peer_manager,
                                            const gchar         *sender,
                                            GDBusConnection     *connection,
                                            const gchar         *action_id,
                                            PolkitDetails       *details,
                                            GCancellable        *cancellable,
                                            GAsyncReadyCallback  callback,
                                            gpointer             user_data)
{
  VddPeerManager *self = VDD_PEER_MANAGER (peer_manager);
  g_autoptr (GTask) task = NULL;
  g_autoptr (AuthorizationCheckData) data = NULL;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, vdd_peer_manager_check_authorization_async);

  data = g_new0 (AuthorizationCheckData, 1);
  data->sender = g_strdup (sender);
  data->connection = g_object_ref (connection);
  data->action_id = g_strdup (action_id);
  data->details = (details != NULL) ? g_object_ref (details) : details;

  g_task_set_task_data (task, g_steal_pointer (&data),
                        (GDestroyNotify) authorization_check_data_free);

  csr_peer_manager_ensure_peer_info_async (peer_manager, sender, connection,
                                           self->cancellable,
                                           check_authorization_cb1,
                                           g_steal_pointer (&task));
}

static void
check_authorization_cb1 (GObject      *obj,
                         GAsyncResult *result,
                         gpointer      user_data)
{
  VddPeerManager *self = VDD_PEER_MANAGER (obj);
  g_autoptr (GTask) task = G_TASK (user_data);
  AuthorizationCheckData *data;
  g_autoptr (GError) error = NULL;
  /*TODO
   * g_autoptr (PolkitSubject) subject = NULL;
   * g_autoptr (CbyProcessInfo) process_info = NULL;
   * */
  data = g_task_get_task_data (task);

  /* Get the peer’s info. */
  csr_peer_manager_ensure_peer_info_finish (CSR_PEER_MANAGER (obj),
                                                           result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  //TODO
  #if 0
  /* FIXME: Do we want to do other authorisation checks against
   * @process_info? */
  if (cby_process_info_get_process_type (process_info) ==
      CBY_PROCESS_TYPE_UNKNOWN)
    {
      g_task_return_new_error (task, G_DBUS_ERROR,
                               G_DBUS_ERROR_ACCESS_DENIED,
                               _("Unknown-type processes cannot access vehicle "
                                 "data."));
      return;
    }

  #endif

  /* Check the peer’s authorisation. */
  {
    g_auto (GStrv) keys = NULL;
    g_autofree gchar *formatted_details = NULL;

    keys = polkit_details_get_keys (data->details);
    if (keys != NULL)
      formatted_details = g_strjoinv (", ", keys);
    else
      formatted_details = g_strdup_printf ("(none)");

    DEBUG ("Checking authorisation for subject ‘%s’ to do action ‘%s’ "
           "with details: %s", data->sender, data->action_id,
           formatted_details);
  }

  /* FIXME: It might be desirable to simply implement #PolkitSubject on
   * #CbyProcessInfo. This won’t work if we’re running on the session bus.
   * */

  /*TODO
  subject = polkit_system_bus_name_new (data->sender);
  */

  polkit_authority_check_authorization (self->authority, NULL,
                                        data->action_id, data->details,
                                        POLKIT_CHECK_AUTHORIZATION_FLAGS_NONE,
                                        self->cancellable,
                                        check_authorization_cb2,
                                        g_steal_pointer (&task));
}

static void
check_authorization_cb2 (GObject      *obj,
                         GAsyncResult *result,
                         gpointer      user_data)
{
  g_autoptr (GTask) task = G_TASK (user_data);
  /* TODO
    g_autoptr (PolkitAuthorizationResult) auth_result = NULL;
   * */
  g_autoptr (GError) error = NULL;

  //TODO

  #if 0
  auth_result = polkit_authority_check_authorization_finish (POLKIT_AUTHORITY (obj),
                                                             result, &error);

  /* This may fail if polkit is not running, amongst other reasons. */
  if (error != NULL)
    {
      DEBUG ("Error: %s", error->message);
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  DEBUG ("Result: authorized: %s, dismissed: %s, is-challenge: %s",
         polkit_authorization_result_get_is_authorized (auth_result) ? "yes" : "no",
         polkit_authorization_result_get_dismissed (auth_result) ? "yes" : "no",
         polkit_authorization_result_get_is_challenge (auth_result) ? "yes" : "no");

  /* Handle various authorisation denials. */
  if (!polkit_authorization_result_get_is_authorized (auth_result))
    {
      if (polkit_authorization_result_get_dismissed (auth_result))
        g_task_return_new_error (task, G_DBUS_ERROR,
                                 G_DBUS_ERROR_ACCESS_DENIED,
                                 _("Action dismissed by the user."));
      else if (polkit_authorization_result_get_is_challenge (auth_result))
        g_task_return_new_error (task, G_DBUS_ERROR,
                                 G_DBUS_ERROR_ACCESS_DENIED,
                                 _("Authorization challenge required."));
      else
        g_task_return_new_error (task, G_DBUS_ERROR,
                                 G_DBUS_ERROR_ACCESS_DENIED,
                                 _("Action not authorized."));

      return;
    }

  #endif
  g_task_return_boolean (task, TRUE);
}

static void
vdd_peer_manager_check_authorization_finish (CsrPeerManager  *peer_manager,
                                             GAsyncResult    *result,
                                             GError         **error)
{
  GTask *task = G_TASK (result);

  g_return_if_fail (g_task_is_valid (result, peer_manager));

  g_task_propagate_boolean (task, error);
}
